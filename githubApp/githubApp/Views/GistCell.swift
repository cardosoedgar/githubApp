//
//  GistCell.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit
import Cartography
import Kingfisher

class GistCell: UICollectionViewCell, ReusableCell, ViewCode {
    let image = UIImageView()
    let name = UILabel()
    let type = UILabel()
    
    func setup(gist: Gist?) {
        guard let gist = gist else {
            return
        }
        
        let imageUrl = URL(string: gist.owner.avatar)
        image.kf.setImage(with: imageUrl)
        name.text = gist.owner.name
        type.text = gist.file.type
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        constrain(self, image, name, type) { containerView, image, name, type in
            containerView.height == 56
            
            image.left == containerView.leftMargin + 8
            image.centerY == containerView.centerY
            image.height == 40
            image.width == 40
            
            name.left == image.right + 8
            name.right == containerView.rightMargin - 8
            name.top == image.top
            
            type.left == image.right + 8
            type.right == containerView.rightMargin - 8
            type.top == name.bottom + 5
        }
    }
    
    func buildViewHierarchy() {
        self.addSubview(image)
        self.addSubview(name)
        self.addSubview(type)
    }
    
    func configureViews() {
        self.backgroundColor = .white
        
        name.numberOfLines = 1
        name.textColor = .black
        name.font = UIFont.boldSystemFont(ofSize: 16)
        
        type.numberOfLines = 1
        type.textColor = .gray
        type.font = UIFont.systemFont(ofSize: 12)
    }
}
