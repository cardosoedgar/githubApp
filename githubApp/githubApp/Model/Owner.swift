//
//  Owner.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class Owner {
    let name: String
    let avatar: String
    
    init?(json: JsonObject) {
        guard let name = json["login"] as? String,
            let avatar = json["avatar_url"] as? String else {
                return nil
        }
        
        self.name = name
        self.avatar = avatar
    }
}
