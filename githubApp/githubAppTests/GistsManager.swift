//
//  GistsManager.swift
//  githubAppTests
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick

@testable import githubApp

class GistsManagerTest: QuickSpec {
    override func spec() {
        describe("GistManager") {
            
            var gistManager: GistsManager!
            let githubAPI = GithubRequest()
            var json: Json? = nil
            
            beforeEach {
                gistManager = GistsManager()
                gistManager.githubAPI = githubAPI
                json = Json(json: JsonHelper.readJson(name: "gists"))
            }
            
            it("should load gist") {
                githubAPI.networkRequest = NetworkRequestMock(json: json)
                var success = false
                
                gistManager.loadGist() { result in
                    success = result
                }
                
                expect(success).toEventually(beTrue())
            }
            
            it("should return false when an error occur on loadGist method") {
                githubAPI.networkRequest = NetworkRequestMock(error: true)
                var success = true

                gistManager.loadGist() { result in
                    success = result
                }

                expect(success).toEventually(beFalse())
            }

            it("should give gist at Index") {
                githubAPI.networkRequest = NetworkRequestMock(json: json)
                var gist: Gist? = nil

                gistManager.loadGist() { success in
                    gist = gistManager.gistAt(index: 0)
                }

                expect(gist).toEventuallyNot(beNil())
            }

            it("should give nil to unkown index") {
                githubAPI.networkRequest = NetworkRequestMock(json: json)
                var gist: Gist? = Gist(json: [String:Any]())

                gistManager.loadGist() { success in
                    gist = gistManager.gistAt(index: 100)
                }

                expect(gist).toEventually(beNil())
            }
            
            it("should filter the array of gists and then clear to return back to the original array") {
                githubAPI.networkRequest = NetworkRequestMock(json: json)
                
                gistManager.loadGist() { success in
                    expect(gistManager.gistsCount()) == 20
                    
                    gistManager.filterGistsOwner(with: "ko1")
                    
                    expect(gistManager.gistsCount()) == 2
                    
                    gistManager.clearFilter()
                    
                    expect(gistManager.gistsCount()) == 20
                }
            }
            
            it("should append when requesting other page then first one") {
                githubAPI.networkRequest = NetworkRequestMock(json: json)
                
                gistManager.loadGist() { success in
                    
                    expect(gistManager.gistsCount()) == 20
                    
                    gistManager.loadGist(page: 1) { success in
                        expect(gistManager.gistsCount()) == 40
                    }
                }
            }
            
            it("should get gist in filtered array") {
                githubAPI.networkRequest = NetworkRequestMock(json: json)
                
                gistManager.loadGist() { success in
                    gistManager.filterGistsOwner(with: "ko1")
                    let gist = gistManager.gistAt(index: 1)
                    expect(gist?.file.type) == "application/json"
                }
            }
        }
    }
}
