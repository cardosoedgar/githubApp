//
//  ViewController.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit
import Cartography

class MainViewController: UIViewController, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let refreshControl = UIRefreshControl()
    let searchBar = UISearchBar()
    
    var didFinishLoading = true
    var page = 0
    var gistManager = GistsManager()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        title = "Gists"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupPullToRefresh()
        loadGists()
    }

    func setupViews() {
        self.view.backgroundColor = .white
        self.view.addSubview(self.searchBar)
        self.view.addSubview(self.collectionView)
        setupSearchBar()
        setupCollectionView()
        setupConstraints()
    }
    
    func setupSearchBar() {
        searchBar.delegate = self
        searchBar.accessibilityIdentifier = "SEARCH_BAR"
        searchBar.placeholder = "search gist owner"
        searchBar.barStyle = .default
        searchBar.showsCancelButton = true
        searchBar.sizeToFit()
    }
    
    func setupCollectionView() {
        collectionView.backgroundColor = .white
        collectionView.accessibilityIdentifier = "COLLECTION_VIEW"
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GistCell.self, forCellWithReuseIdentifier: GistCell.reuseIdentifier)
    }
    
    func setupConstraints() {
        constrain(self.view, self.searchBar ,self.collectionView) { containerView, searchBar, collectionView in
            searchBar.trailing == containerView.trailing
            searchBar.leading == containerView.leading
            searchBar.height == 40
            searchBar.top == containerView.topMargin
            
            collectionView.top == searchBar.bottom
            collectionView.trailing == containerView.trailing
            collectionView.leading == containerView.leading
            collectionView.bottom == containerView.bottomMargin
        }
    }
    
    func loadGists() {
        gistManager.loadGist(page: page) { success in
            if success {
                self.collectionView.reloadData()
            } else {
                self.showAlert(withTitle: "Error", andMessage: "There was a error trying to load gists.")
            }

            self.didFinishLoading = true
            self.refreshControl.endRefreshing()
        }
    }

    //MARK: Collection View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let gist = gistManager.gistAt(index: indexPath.row)
        let vc = DetailViewController(gist: gist)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gistManager.gistsCount()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(for: indexPath) as GistCell
        let gist = gistManager.gistAt(index: indexPath.row)
        
        cell.setup(gist: gist)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width
        return CGSize(width: width, height: 56)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    //MARK: Scroll Delegate / Pull to Refresh
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            if self.didFinishLoading {
                self.didFinishLoading = false
                self.page += 1
                self.loadGists()
                self.collectionView.reloadData()
            }
        }
    }

    func setupPullToRefresh() {
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }

        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }

    @objc func pullToRefresh() {
        self.page = 0
        self.loadGists()
    }
    
    //MARL: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            gistManager.filterGistsOwner(with: searchText)
        } else {
            gistManager.clearFilter()
        }
        
        collectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        gistManager.clearFilter()
        collectionView.reloadData()
    }
}
