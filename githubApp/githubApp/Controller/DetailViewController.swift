//
//  DetailViewController.swift
//  githubApp
//
//  Created by Edgar Cardoso on 14/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit
import Cartography

class DetailViewController: UIViewController, ViewCode {
    let image = UIImageView()
    let name = UILabel()
    let gist: Gist?
    
    init(gist: Gist?) {
        self.gist = gist
        
        super.init(nibName: nil, bundle: nil)
        title = "Detail"
        view.backgroundColor = .white
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        constrain(self.view, image, name) { containerView, image, name in
            image.top == containerView.topMargin + 20
            image.width == containerView.width - 40
            image.centerX == containerView.centerX
            image.height == image.width
            
            name.top == image.bottom + 20
            name.trailing == containerView.trailing
            name.leading == containerView.leading
        }
    }
    
    func buildViewHierarchy() {
        self.view.addSubview(image)
        self.view.addSubview(name)
    }
    
    func configureViews() {
        guard let gist = gist else {
            return
        }
        
        image.contentMode = .scaleAspectFit
        let imageUrl = URL(string: gist.owner.avatar)
        image.kf.setImage(with: imageUrl)
        
        name.font = UIFont.boldSystemFont(ofSize: 28)
        name.textAlignment = .center
        name.text = gist.owner.name
    }
}
