//
//  NetworkRequestMock.swift
//  githubAppTests
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Alamofire

@testable import githubApp

class NetworkRequestMock: NetworkRequestProtocol {
    
    let json: Json?
    let error: Bool
    
    init(json: Json? = nil, error: Bool = false) {
        self.json = json
        self.error = error
    }
    
    func request(_ url: URL, method: HTTPMethod, parameters: [String : Any]?, headers: [String : String]?, completion: @escaping (githubApp.Result<Json>) -> Void) {
        if error {
            completion(.error)
            return
        }
        
        guard let json = self.json else {
            completion(.error)
            return
        }
        
        completion(.success(json))
    }
}

