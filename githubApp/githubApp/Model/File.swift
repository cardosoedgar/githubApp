//
//  File.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class File {
    let type: String
    
    init?(json: JsonObject) {
        guard let file = json.values.first as? JsonObject,
                let type = file["type"] as? String else {
            return nil
        }
        
        self.type = type
    }
}
