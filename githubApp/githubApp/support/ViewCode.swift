//
//  ViewCode.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit

protocol ViewCode {
    func setupConstraints()
    func buildViewHierarchy()
    func configureViews()
    func setup()
}

extension ViewCode {
    func setup() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}
