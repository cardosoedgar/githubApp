//
//  Gists.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class Gist {
    let owner: Owner
    let file: File
    
    init?(json: JsonObject) {
        guard let ownerJson = json["owner"] as? JsonObject,
            let fileJson = json["files"] as? JsonObject ,
            let owner = Owner(json: ownerJson),
            let file = File(json: fileJson) else {
                return nil
        }
        
        self.owner = owner
        self.file = file
    }
    
    class func load(jsonArray: [JsonObject]) -> [Gist] {
        var gists = [Gist]()
        
        for obj in jsonArray {
            if let gist = Gist(json: obj) {
                gists.append(gist)
            }
        }
        
        return gists
    }
}
