//
//  MainViewController.swift
//  githubAppUITests
//
//  Created by Edgar Cardoso on 15/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick
import KIF

@testable import githubApp

extension GistsManager {
    class func getMockManager(shouldLoad: Bool) -> GistsManager {
        let gistManager = GistsManager()
        let githubAPI = GithubRequest()

        if shouldLoad {
            let json = Json(json: JsonHelper.readJson(name: "gists"))
            githubAPI.networkRequest = NetworkRequestMock(json: json)
        } else {
            githubAPI.networkRequest = NetworkRequestMock(error: true)
        }

        gistManager.githubAPI = githubAPI
        return gistManager
    }
}

class MainViewControllerTest: QuickSpec {
    override func spec() {
        describe("MainViewController") {

            var navController: UINavigationController?
            var mainController: MainViewController!
            
            func shouldLoadGists(shouldLoad: Bool) {
                mainController.gistManager = GistsManager.getMockManager(shouldLoad: shouldLoad)
                mainController.loadGists()
            }
            
            beforeSuite {
                self.tester().acknowledgeSystemAlert()
                mainController = MainViewController()
                mainController.gistManager = GistsManager.getMockManager(shouldLoad: true)
                navController = UINavigationController(rootViewController: mainController)
                
                let window = UIApplication.shared.keyWindow
                window?.rootViewController = navController
                mainController.loadGists()
            }
            
            it("should have 20 gists") {
                shouldLoadGists(shouldLoad: true)
                let count = mainController.collectionView.numberOfItems(inSection: 0)
                expect(count) == 20
            }

            it("should fire an alert when cant load the gists") {
                shouldLoadGists(shouldLoad: false)
                self.tester().waitForView(withAccessibilityLabel: "Error")
                self.tester().tapView(withAccessibilityLabel: "Ok")
            }

            it("should filter gists by owner when typing on searchbar") {
                shouldLoadGists(shouldLoad: true)
                self.tester().clearText(fromAndThenEnterText: "Ko1", intoViewWithAccessibilityIdentifier: "SEARCH_BAR")
                let count = mainController.collectionView.numberOfItems(inSection: 0)
                expect(count) == 2
            }

            it("should clear filter when pressing cancel on searchbar") {
                shouldLoadGists(shouldLoad: true)
                self.tester().clearText(fromAndThenEnterText: "Nbadmin", intoViewWithAccessibilityIdentifier: "SEARCH_BAR")
                var count = mainController.collectionView.numberOfItems(inSection: 0)
                expect(count) == 2

                self.tester().tapView(withAccessibilityLabel: "Cancel")

                count = mainController.collectionView.numberOfItems(inSection: 0)
                expect(count) == 20
            }

            it("should pull to refresh gists") {
                shouldLoadGists(shouldLoad: false)
                mainController.collectionView.reloadData()
                self.tester().tapView(withAccessibilityLabel: "Ok")
                mainController.gistManager = GistsManager.getMockManager(shouldLoad: true)

                self.tester().pullToRefreshView(withAccessibilityIdentifier: "COLLECTION_VIEW")
                let count = mainController.collectionView.numberOfItems(inSection: 0)
                expect(count) == 20
            }

            it("should tap cell and go to detail view controller") {
                shouldLoadGists(shouldLoad: true)
                let indexPath = IndexPath(row: 0, section: 0)
                self.tester().tapItem(at: indexPath, inCollectionViewWithAccessibilityIdentifier: "COLLECTION_VIEW")

                self.tester().waitForView(withAccessibilityLabel: "Detail")
                self.tester().tapView(withAccessibilityLabel: "Gists")
            }
        }
    }
}

