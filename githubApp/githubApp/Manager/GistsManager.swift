//
//  GistsManager.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class GistsManager {
    
    var githubAPI = GithubRequest()
    private var gists = [Gist]()
    private var filtered: [Gist]? = nil
    
    func loadGist(page: Int = 0, completion: @escaping (Bool) -> Void) {
        githubAPI.getGists(page: page) { jsonObject in
            guard let json = jsonObject else {
                completion(false)
                return
            }
            
            if (page == 0) {
                self.gists = Gist.load(jsonArray: json)
            } else {
                self.gists.append(contentsOf: Gist.load(jsonArray: json))
            }
            completion(true)
        }
    }
    
    func gistAt(index: Int) -> Gist? {
        if let filtered = filtered, index >= 0 && index < filtered.count {
            return filtered[index]
        }
        
        if index >= 0 && index < gists.count {
            return gists[index]
        }
        
        return nil
    }
    
    func gistsCount() -> Int {
        if let filtered = filtered {
            return filtered.count
        }
        
        return gists.count
    }
    
    func filterGistsOwner(with text: String) {
        self.filtered = gists.filter { gist in
            return gist.owner.name.containsIgnoringCase(find: text)
        }
    }
    
    func clearFilter() {
        self.filtered = nil
    }
}
