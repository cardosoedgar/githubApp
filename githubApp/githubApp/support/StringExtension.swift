//
//  StringExtension.swift
//  githubApp
//
//  Created by Edgar Cardoso on 14/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

extension String {
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
