//
//  GistTests.swift
//  githubAppTests
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Quick
import Nimble

@testable import githubApp

extension Gist {
    static func validJson() -> JsonObject {
        return [
            "files": [
                "name_of_the_file": [
                    "type": "application/json"
                ]
            ],
            "owner": [
                "login": "github user",
                "avatar_url": "https://avatars1.githubusercontent.com/u/22497503?v=4"
            ]
        ]
    }
}

class GistTests: QuickSpec {
    override func spec() {
        describe("Gist") {
            
            var json: JsonObject!
            
            beforeEach {
                json = Gist.validJson()
            }
            
            it("should parse gist json") {
                let gist = Gist(json: json)
                
                expect(gist?.owner.name) == "github user"
                expect(gist?.owner.avatar) == "https://avatars1.githubusercontent.com/u/22497503?v=4"
                expect(gist?.file.type) == "application/json"
            }
            
            it("should not parse inconsistent json") {
                json.removeValue(forKey: "owner")
                let gist = Gist(json: json)
                
                expect(gist).to(beNil())
            }
            
            it("should parse an array of gists") {
                let array: [JsonObject] = [json, json, json, json]
                
                let gists = Gist.load(jsonArray: array)
                
                expect(gists.count) == 4
            }
        }
    }
}
