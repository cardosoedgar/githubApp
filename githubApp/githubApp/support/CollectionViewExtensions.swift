//
//  CollectionViewExtensions.swift
//  githubApp
//
//  Created by Edgar Cardoso on 13/11/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit

public protocol ReusableCell: class {
    static var reuseIdentifier: String { get }
}

public extension ReusableCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

public extension UICollectionView {
    final func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self)
        -> T where T: ReusableCell {
            let dequeueCell = self.dequeueReusableCell(withReuseIdentifier: cellType.reuseIdentifier, for: indexPath)
            guard let cell = dequeueCell as? T else {
                fatalError()
            }
            return cell
    }
}
